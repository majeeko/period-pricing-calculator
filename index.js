(function (root, factory) {
	if (typeof define === "function" && define.amd) {
		define(["lodash", "moment"], factory);
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory(require("lodash"), require("moment"));
	} else {
		if (root) {

			root.periodPricingCalculator = factory(root._, root.moment);
		}
		else if (window) {

			window.periodPricingCalculator = factory(window._, window.moment);
		}
		else {

			global.periodPricingCalculator = factory(global._, global.moment);
		}
	}
}(this, function (_, moment) {

	function _single_date_price(day, pricing) { // day is a moment obj
		for (var i = 0; i < pricing.periods.length; i++) {
			var pricing_period = pricing.periods[i];
			if (day.isBetween(moment(pricing_period.start_date), moment(pricing_period.end_date), 'day', '[)')) {
				return pricing_period.price;
			}
		}
		return pricing.default_price;
	}

	function _calculate_total(period_start, period_end, pricing, single_date_pricing_fn) {
		period_start = moment(period_start);
		period_end = moment(period_end);

		if (!period_start.isValid() || !period_end.isValid() || !period_start.isBefore(period_end)) {
			return null;
		}

		var total = 0;

		var day = moment(period_start).clone();
		while (day.isBefore(moment(period_end))) {
			total += parseFloat(single_date_pricing_fn(day, pricing));
			day.add(1, 'day');
		}

		return total;
	}

	function Calculator(pricing) {
		var single_date_price = _.memoize(function (day) {
			return _single_date_price(day, pricing);
		}, function (day) {
			return day.toISOString()
		});

		return {
			calculate_total: function calculate_total(period_start, period_end) {
				return _calculate_total(period_start, period_end, pricing, single_date_price);
			}
		}
	}

	function clean_price(raw_price) {
		if(!raw_price)
			return 0;
		return parseFloat(raw_price.toString().replace(/,/gi, "."));
	}

	return {
		Calculator: Calculator,
		calculate_total: function calculate_total(period_start, period_end, pricing) {
			return _calculate_total(period_start, period_end, pricing, _single_date_price);
		},
		normalize_pricing: function(pricing){
			return {
				default_price: clean_price(pricing.price),
				periods: (pricing['events']).concat(pricing['seasons'] || []).map(function(p){
					return {
						price: clean_price( p.price),
						start_date: moment(p.start_date, "YYYY-MM-DD"),
						end_date: moment(p.end_date, "YYYY-MM-DD")
					}
				})
			}
		}
	}
}));
