const expect = require('chai').expect;


const moment = require('moment');

const period_pricing_calculator = require('../index');

describe('Calculate demo pricing', () => {
	const traditional_pricing = {
		price: 40,
		events: [
			{
				name: "Millennium Bug Days", // name parameter won't be considered
				price: "101,99",
				start_date: "1999-12-30",
				end_date: "2000-01-01"
			}
		],
		seasons: [
			{
				name: "Winter Time 2000",
				price: 60.9,
				start_date: "2000-01-01",
				end_date: "2000-03-15"
			}
		]
	}

	let period_start_date 	= moment("1999-12-28", "YYYY-MM-DD").toDate()
	let period_end_date 	= moment("2000-01-03", "YYYY-MM-DD").toDate()

	// total should be:
	// 12-28 -> 12-29 : 40		- Basic price
	// 12-29 -> 12-30 : 40		- Basic price
	// 12-30 -> 12-31 : 101.99	- Millennium Bug Days
	// 12-31 -> 01-01 : 101.99	- Millennium Bug Days
	// 01-01 -> 01-02 : 60.90	- Winter Time 2000
	// 01-02 -> 01-03 : 60.90	- Winter Time 2000
	//          TOTAL : 405.78

	let normalized_pricing = period_pricing_calculator.normalize_pricing(traditional_pricing);

	it("Should correctly normalize the pricing", function () {
		expect(normalized_pricing.default_price).to.equal(40);
		expect(normalized_pricing.periods.length).to.equal(2);
		expect(normalized_pricing.periods[0].price).to.equal(101.99);
		expect(normalized_pricing.periods[0].start_date.toISOString()).to.equal(moment("1999-12-30").toISOString());
	});
	it("Should calculate the total price between multiples seasons using the factory", function () {
		let Calculator = period_pricing_calculator.Calculator(normalized_pricing);
		expect(Calculator.calculate_total(period_start_date, period_end_date)).to.equal(405.78)
		expect(Calculator.calculate_total(period_start_date, new Date(2000, 0, 3))).to.equal(405.78)
	});
	it("Should calculate the total price between multiples seasons using the direct calculator", function () {
		expect(period_pricing_calculator.calculate_total(period_start_date, period_end_date, normalized_pricing)).to.equal(405.78)
	});

});
