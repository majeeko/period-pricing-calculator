var gulp = require('gulp');
var uglify = require('gulp-uglify');

// Uglify JS
gulp.task('uglify', function(){
	gulp.src("./index.js")
		.pipe(uglify())
		.pipe(gulp.dest("./dist"));
});


gulp.task('default', function(){
	gulp.watch("./index.js", function(event){
		gulp.run('uglify');
	});
});
