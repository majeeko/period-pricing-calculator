#period-pricing-calculator
> Calculate the total price for a range of dates given a series of pricing periods.

The pricing is defined by:
- an array of peroids (typically they would be seasonal rates), sorted by priority
- a default price

Given a range of dates, the script will calculate the total price for all the nights in the date range.

```javascript
var pricing = {
    default_price: 94.90,
    periods: [
        {
            price: 150,
            start_date: new Date(1999, 11, 29),
            end_date: new Date(2000, 0, 2)
        },
        {
            price: 120,
            start_date: new Date(1999, 10, 15),
            end_date: new Date(2000, 2, 15)
        }
    ]
};
```

The first period will have an higher priority than the second.


The script can be called in 2 ways:
- `Calculator(pricing).calculate_total(period_start, period_end)` for better performance on multiple calculations done using the same `pricing` all the times
- `calculate_total(period_start, period_end, pricing)`